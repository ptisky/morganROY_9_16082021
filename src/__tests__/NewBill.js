import { screen, fireEvent } from "@testing-library/dom";
import { localStorageMock } from "../__mocks__/localStorage.js";
import { ROUTES } from "../constants/routes";
import firestore from "../app/Firestore";
import firebase from "../__mocks__/firebase";
import { bills } from "../fixtures/bills.js";
import NewBillUI from "../views/NewBillUI.js";
import NewBill from "../containers/NewBill.js";

describe("Given I am connected as an employee", () => {
    describe("When I am on NewBill Page", () => {
        //=====---------RUN TEST Add new file---------=====//
        test("Then i add a file", () => {
            //get route
            const onNavigate = (pathname) => {
                document.body.innerHTML = ROUTES({ pathname });
            };

            Object.defineProperty(window, "localStorage", {
                value: localStorageMock,
            });

            //create a valid user
            const user = {
                type: "Employee",
                email: "johndoe@email.com",
            };

            //set user into local storage
            localStorage.setItem("user", JSON.stringify(user));

            //DOM newbill
            document.body.innerHTML = NewBillUI();

            firestore.bills = () => ({
                bills,
                get: jest.fn().mockResolvedValue(),
                add: (bill) => {
                    bills.push(bill);
                    return Promise.resolve();
                },
            });

            //New object newbill
            const newBill = new NewBill({
                document,
                onNavigate,
                firestore: firestore,
                localStorage: window.localStorage,
            });

            //if put a news image
            const handleChangeFile = jest.fn(newBill.handleChangeFile);
            const inputFile = screen.getByTestId("file");

            const newFile = new File(["anImage.png"], "anImage.png", {
                type: "image/png",
            });
            inputFile.addEventListener("change", handleChangeFile);
            fireEvent.change(inputFile, {
                target: {
                    files: [newFile],
                },
            });

            expect(handleChangeFile).toHaveBeenCalled();
            expect(inputFile.files[0].name).toBe(newFile.name);
        });
        //=====---------End RUN TEST Add new file---------=====//
        //===============================================//
        //=====---------RUN TEST Add new bill---------=====//
        test("Then it should create a new bill", () => {
            //get route
            const onNavigate = (pathname) => {
                document.body.innerHTML = ROUTES({ pathname });
            };

            Object.defineProperty(window, "localStorage", {
                value: localStorageMock,
            });

            //create a valid user
            const user = {
                type: "Employee",
                email: "johndoe@email.com",
            };

            //set user into local storage
            localStorage.setItem("user", JSON.stringify(user));

            //Get DOM newbill
            const html = NewBillUI();
            document.body.innerHTML = html;

            firestore.bills = () => ({
                bills,
                get: jest.fn().mockResolvedValue(),
                add: (bill) => {
                    bills.push(bill);
                    return Promise.resolve();
                },
            });

            //news oject newsBill
            const newBill = new NewBill({
                document,
                onNavigate,
                firestore: firestore,
                localStorage: window.localStorage,
            });

            //If submit newBill
            const handleSubmit = jest.fn(newBill.handleSubmit);
            const submitBtn = screen.getByTestId("form-new-bill");
            submitBtn.addEventListener("submit", handleSubmit);
            fireEvent.submit(submitBtn);
            expect(handleSubmit).toHaveBeenCalled();
        });
        //=====---------End RUN TEST Add new bill---------=====//
        //===============================================//
        //=====---------RUN TEST NEW BILL---------=====//
        test("then a bill is created", () => {
            //get route
            const onNavigate = (pathname) => {
                document.body.innerHTML = ROUTES({ pathname });
            };

            //append news bill html
            document.body.innerHTML = NewBillUI();

            firestore.bills = () => ({
                bills,
                get: jest.fn().mockResolvedValue(),
                add: (bill) => {
                    bills.push(bill);
                    return Promise.resolve();
                },
            });

            //New object new bill
            const newBill = new NewBill({
                document,
                onNavigate,
                firestore: firestore,
                localStorage: window.localStorage,
            });

            //create a validbill config
            const validBill = {
                name: "Facture test",
                fileName: "testImage.jpg",
                date: "2011-05-19",
                type: "Restaurants et bars",
                vat: "60",
                amount: 100,
                pct: 20,
                commentary: "Test comment",
            };

            //run method to create bill
            newBill.createBill = (newBill) => newBill;

            //html value of the new bill
            document.querySelector(`input[data-testid="expense-name"]`).value =
                validBill.name;
            document.querySelector(`input[data-testid="datepicker"]`).value =
                validBill.date;
            document.querySelector(`select[data-testid="expense-type"]`).value =
                validBill.type;
            document.querySelector(`input[data-testid="amount"]`).value =
                validBill.amount;
            document.querySelector(`input[data-testid="vat"]`).value =
                validBill.vat;
            document.querySelector(`input[data-testid="pct"]`).value =
                validBill.pct;
            document.querySelector(`textarea[data-testid="commentary"]`).value =
                validBill.commentary;
            newBill.fileName = validBill.fileName;

            //onSubmit
            const handleSubmit = jest.fn((e) => newBill.handleSubmit(e));
            const submit = screen.getByTestId("form-new-bill");
            submit.addEventListener("click", handleSubmit);
            fireEvent.click(submit);
            expect(handleSubmit).toHaveBeenCalled();
        });
        //=====---------End RUN TEST NEW BILL---------=====//

        //=====---------RUN TEST Error 400 & 500---------=====//

        //=====---------end RUN TEST Error 400 & 500---------=====//
    });
});
