import { screen, fireEvent } from "@testing-library/dom";
import BillsUI from "../views/BillsUI.js";
import Bills from "../containers/Bills";
import { bills } from "../fixtures/bills.js";
import Router from "../app/Router";
import firestore from "../app/Firestore";
import { ROUTES, ROUTES_PATH } from "../constants/routes.js";
import firebase from "../__mocks__/firebase";
import { localStorageMock } from "../__mocks__/localStorage.js";

describe("Given I am connected as an employee", () => {
    describe("When I am on Bills Page", () => {
        //=====---------RUN TEST icon eye---------=====//
        test("Then bill icon in vertical layout should be highlighted", () => {
            //create a valid user
            const user = {
                type: "Employee",
                email: "johndoe@email.com",
            };

            //set user into local storage
            localStorage.setItem("user", JSON.stringify(user));

            const rootElem = document.createElement("div");
            rootElem.id = "root";
            document.body.appendChild(rootElem);

            firestore.bills = () => ({
                bills,
                get: jest.fn().mockResolvedValue(),
            });

            Object.defineProperty(window, "location", {
                value: {
                    hash: ROUTES_PATH["Bills"],
                },
            });

            Router();

            const iconWindow = screen.getByTestId("icon-window");
            expect(iconWindow.classList.contains("active-icon")).toBe(true);
        });
        //=====---------end RUN TEST icon eye---------=====//
        //=====---------RUN TEST bills early to lastest---------=====//
        test("Then bills should be ordered from earliest to lastest", () => {
            const html = BillsUI({ data: bills });
            document.body.innerHTML = html;
            const dates = screen
                .getAllByText(
                    /^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/i
                )
                .map((a) => a.innerHTML);
            const antiChrono = (a, b) => (a < b ? 1 : -1);
            const datesSorted = [...dates].sort(antiChrono);
            expect(dates).toEqual(datesSorted);
        });
        //=====---------end RUN TEST bills early to lastest---------=====//
    });
});
describe("Given I am connected as Employee on Bills page", () => {
    describe("When new bill", () => {
        //=====---------RUN TEST new bill redirect---------=====//
        test("Then go to newbill page", () => {
            //get route
            const onNavigate = (pathname) => {
                document.body.innerHTML = ROUTES({ pathname });
            };

            Object.defineProperty(window, "localStorage", {
                value: localStorageMock,
            });

            //create a valid user
            const user = {
                type: "Employee",
                email: "johndoe@email.com",
            };

            //set user into local storage
            localStorage.setItem("user", JSON.stringify(user));

            //DOM Bill
            document.body.innerHTML = BillsUI({ data: [] });

            firestore.bills = () => ({
                bills,
                get: jest.fn().mockResolvedValue(),
                add: (bill) => {
                    bills.push(bill);
                    return Promise.resolve();
                },
            });

            const allBills = new Bills({
                document,
                onNavigate,
                firestore: firestore,
                localStorage: window.localStorage,
            });

            //When click new bill
            const handleClickNewBill = jest.fn(allBills.handleClickNewBill);
            const billBtn = screen.getByTestId("btn-new-bill");
            billBtn.addEventListener("click", handleClickNewBill);
            fireEvent.click(billBtn);
            expect(screen.queryByTestId("form-new-bill")).toBeTruthy();
        });
        //=====---------end RUN TEST new bill redirect---------=====//
    });

    describe("When I click on view", () => {
        //=====---------RUN TEST open modal---------=====//
        test("then open modal", () => {
            //get route
            const onNavigate = (pathname) => {
                document.body.innerHTML = ROUTES({ pathname });
            };

            Object.defineProperty(window, "localStorage", {
                value: localStorageMock,
            });

            //create a valid user
            const user = {
                type: "Employee",
                email: "johndoe@email.com",
            };

            //set user into local storage
            localStorage.setItem("user", JSON.stringify(user));

            //DOM bills
            document.body.innerHTML = BillsUI({ data: bills });

            firestore.bills = () => ({
                bills,
                get: jest.fn().mockResolvedValue(),
                add: (bill) => {
                    bills.push(bill);
                    return Promise.resolve();
                },
            });

            //New object newbill
            const BillsObj = new Bills({
                document,
                onNavigate,
                firestore: firestore,
                localStorage: window.localStorage,
            });

            $.fn.modal = jest.fn();
            const icon = screen.getAllByTestId("icon-eye")[0];
            const handleClickIconEye = jest.fn(() =>
                BillsObj.handleClickIconEye(icon)
            );
            icon.addEventListener("click", handleClickIconEye);
            fireEvent.click(icon);
            expect(handleClickIconEye).toHaveBeenCalled();
            const modalToOpen = document.getElementById("modaleFile");
            expect(modalToOpen).toBeTruthy();
        });
        //=====---------end RUN TEST open modal---------=====//
    });

    describe("When I navigate to dashboard", () => {
        //=====---------RUN TEST Error 400 & 500---------=====//
        test("fetches bills from mock API GET", async () => {
            const getSpy = jest.spyOn(firebase, "get");
            const bills = await firebase.get();
            expect(getSpy).toHaveBeenCalledTimes(1);
            expect(bills.data.length).toBe(4);
        });
        test("fetches bills from an API and fails with 404 message error", async () => {
            firebase.get.mockImplementationOnce(() =>
                Promise.reject(new Error("Erreur 404"))
            );
            const html = BillsUI({ error: "Erreur 404" });
            document.body.innerHTML = html;
            const message = await screen.getByText(/Erreur 404/);
            expect(message).toBeTruthy();
        });
        test("fetches messages from an API and fails with 500 message error", async () => {
            firebase.get.mockImplementationOnce(() =>
                Promise.reject(new Error("Erreur 500"))
            );
            const html = BillsUI({ error: "Erreur 500" });
            document.body.innerHTML = html;
            const message = await screen.getByText(/Erreur 500/);
            expect(message).toBeTruthy();
        });
        //=====---------end RUN TEST Error 400 & 500---------=====//
    });
});
